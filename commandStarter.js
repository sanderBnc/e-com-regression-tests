var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var parseString = require('xml2js').parseString;
const execSync = require('child_process').execSync;
var fs = require('fs');

let output = null;
var hour = new Date().getHours();
var browsers = process.argv[2]; //chrome,firefox,etc..

// if nightly scheduled build or deploy build during the day
if(hour >= 2 && hour <= 6) { //test all
    try{
        output = execSync('testcafe ' + browsers + ' ComponentTests --hostname localhost --reporter nunit:components.xml -S -s ecomSS', { encoding: 'utf-8' });
        output = execSync('testcafe ' + browsers + ' bugsTests --hostname localhost --reporter nunit:bugs.xml -S -s ecomSS', { encoding: 'utf-8' });
        output = execSync('testcafe ' + browsers + ' userStories --hostname localhost --reporter nunit:reportEcom.xml -S -s ecomSS', { encoding: 'utf-8' });
    }catch(err){
        if(err && err.code == 2){
            console.log("exception: "+err);
            console.log("out: "+err.stdout);
            console.log("exception: "+err.stderr);
        }
       
    }
} else { //starts specific tests
    try{
        output = execSync('testcafe ' + browsers + ' userStories --hostname localhost --reporter nunit:reportEcom.xml -S -s ecomSS', { encoding: 'utf-8' });
        //console.log(output);
    }catch(err){
        if(err && err.code == 2){
            console.log("exception: "+err);
            console.log("out: "+err.stdout);
            console.log("exception: "+err.stderr);
        }
       
    }
    XMLHttpRequestBamboo();
}


//parse bamboo response and start tests
function reqListener () {
    //console.log(this.responseText);
    let keys = [];
    let components = [];
    var xml = this.responseText;
    parseString(xml, function (err, result) {
        //console.log(JSON.stringify(result));
        //let obj = JSON.parse(JSON.stringify(result));
        //console.log(result.results.results[0].result[0].jiraIssues[0].issue);
        let arrayOfKeys = result.results.results[0].result[0].jiraIssues[0].issue;
        let componentArray = '';
        for (let child in arrayOfKeys){
            console.log(arrayOfKeys[child].$.key);
            let xhrObj = XMLHttpRequestJira(arrayOfKeys[child].$.key);
            componentArray = xhrObj.components;
            components.concat(componentArray);
            if(xhrObj.keyStatus === "Verify / Test"){
                keys.push(arrayOfKeys[child].$.key);
            }else{
                console.log('status of ' + arrayOfKeys[child].$.key + " is not Verify / Test, but " + xhrObj.keyStatus);
            }
        }
    });
    components = new Set(components);
    components = Array.from(components);
    console.log('components: ' + components);
    console.log('keys:' + keys);
    execTests(components, 'ComponentTests');
    execTests(keys, 'bugsTests');
}

//send an XHR to the Bamboo api and returns all issues linked to the latest build
function XMLHttpRequestBamboo(){
    //test short
    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", reqListener);
    oReq.open("GET", "https://bamboo.bnc.nl/rest/api/latest/result/BNCEC-ECOMFE/?expand=results.result.jiraIssues", true, 'jira-issue-getter', 'Test1234');
    oReq.send();
}

//send an XHR to the Jira api and returns components and status of an issue 
function XMLHttpRequestJira(key) {
    var oReq2 = new XMLHttpRequest();
    oReq2.open("GET", "https://bncdev.atlassian.net/rest/api/latest/issue/"+ key, false, 'sander.vanderhaak@bnc.nl', 'Test1234');
    let components = '';
    oReq2.onreadystatechange = function () {
        components = parseComponent(oReq2.responseText);
        keyStatus = parseKey(oReq2.responseText);
    }
    oReq2.send();
    return {components: components, keyStatus: keyStatus};
}

//parse the components out of a jira issue json response
function parseComponent (text) {
    var JSONtext = text;
    //console.log(JSONtext);
    let obj = JSON.parse(JSONtext);
    let component = null;
    if (obj.fields.components.length > 0){
        for (let child in obj.fields.components){
            console.log('Test: ' + obj.fields.components[child].name);
            component = obj.fields.components[child].name;
            component = component.replace(/ /g, '-');
        }   
    }else{
        console.log('no components linked');
    }
    return component;
}

//parse the status name out a jira issue json response
function parseKey(text) {
    var JSONtext = text;
    //console.log(JSONtext);
    let obj = JSON.parse(JSONtext);
    let key = obj.fields.status.name;
    console.log('Test: ' + obj.fields.status.name);   
    return key;
}


//executes tests in a specific map/folder with the same names as the tests array has 
function execTests(tests, map){
    for (let i in tests){
        console.log(tests[i], i);
        if (fs.existsSync('./' + map + '/' + tests[i] + '.js') && tests[i] !== null) {
            console.log('testcafe ' + browsers + ' ' + map + '/' + tests[i] + '.js --hostname localhost --reporter nunit:' + tests[i] + '.xml');
            try{
                output = execSync('testcafe ' + browsers + ' ' + map + '/' + tests[i] + '.js --hostname localhost --reporter nunit:' + tests[i] + '.xml', { encoding: 'utf-8' });
                output = execSync('testcafe ' + browsers + ' '  + map + '/' + tests[i] + '.js --hostname localhost --reporter nunit:' + tests[i] + '.xml', { encoding: 'utf-8' });
            }catch(err){
                if(err && err.code == 2){
                    console.log("exception: "+err);
                    console.log("out: "+err.stdout);
                    console.log("exception: "+err.stderr);
                }
            }
        } else {
            console.log('No test for: ' + tests[i]);
        }
    }
}


