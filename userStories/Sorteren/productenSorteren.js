//Als webshopgebruiker wil ik producten kunnen sorteren op prijs, artikelcode, producttitel en andere eigenschappen.
import { ClientFunction } from 'testcafe';
import SortObject from './SortObject';

const getWindowLocation = ClientFunction(() => window.location);
const sortPom = new SortObject();

fixture `Product Sorteren`
    .page `https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops`
    .beforeEach( async t => {
        let location = await getWindowLocation();   
        await t
            .expect(location.href).eql('https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops');
    })
    .afterEach( async t => {
        let location = await getWindowLocation();   
        await t
            .expect(location.href).match(/(https:\/\/test-winkel.bnc.nl\/#\/cat\/werkplekken\/Laptops)/);
    });


test('Levertijd', async t => {
    //cijfers voor letters => meer dan 5 klopt nog
    await sortPom.clickSelectSort('Levertijd');
    await sortPom.testSortedBy('.product__delivery', 'name');
});

test('Naam', async t => {
    await sortPom.clickSelectSort('Naam');
    await sortPom.testSortedBy('.pbcTitle', 'name');
});

test('Populariteit', async t => {
    await sortPom.clickSelectSort('Populariteit');
    await sortPom.testSortedBy('.testPopularity', 'number');
});

test('prijs l-h', async t => {
    await sortPom.clickSelectSort('Prijs laag-hoog');
    await sortPom.testSortedBy('.testPrice', 'number');
});

test('prijs h-l', async t => {
    await sortPom.clickSelectSort('Prijs hoog-laag');
    await sortPom.testSortedBy('.testPrice', 'rNumber');
});

test('productcode', async t => {
    await sortPom.clickSelectSort('Productcode');
    await sortPom.testSortedBy('.testProductCode', 'name');
});

test('Meest door mij besteld', async t => {
    await sortPom.clickSelectSort('Meest door mij besteld');
    await sortPom.testSortedBy('.testOrderedByMeCount', 'number');
});
