import { Selector, t } from 'testcafe';

class Item {
    constructor(name, amount){
        this.name = name.toLowerCase().replace(/\n/g,'').replace(/ /g,'');
        this.amount = parseInt(amount);
    }
}

//winkelwagen object to keep track of the items in the cart 
export default class WinkelwagenObject {
    constructor () {
        this.items = [  new Item('Playstation4', 7),
                        new Item('LaptopMaginfique2000', 2),
                        new Item('AcerAspireEX21', 1),
                        new Item('LenovoTest123', 4),
                        new Item('MacbookPro3miljardinch', 7),
                        new Item('combivoordeel:playstation4ipadpro(3x)ipodnano(3x)', 2)];
    }

    //add a bundle to the cart
    async addBundle(nthNumber, amount){
        // GET TITLE COMBINATIE: X   
        let bundle = await Selector('.bundleProduct').nth(nthNumber);
        let bundleItems = await bundle.find('.bundleProduct__product');
        let bundleItemsCount = await bundleItems.count;

        await t.typeText(bundle.find('.product__quantityInput'), amount.toString(), { replace: true });
        let name = 'combivoordeel:';
        for (let i = 0; i < await bundleItemsCount; i++){
            let title = bundleItems.nth(i).find('.product__title');
            name += await title.innerText;
            let quantity = await bundleItems.nth(i).find('.bundleCount');
            if (await quantity.exists){
                await t.expect(await quantity.innerText).notEql('');
                //console.log(await quantity.innerText);
                name += ' (' + await quantity.innerText + ')';
            }
        }
        //add if new item or add to other products
        this.addItemToObject(name, amount);

        //click add
        await t.click(bundle.find('.product__plusButton'));

        //check if added correctly
        await this.checkBasketPopup();
    }

    //add 'amount' of an item to the cart based on being nth() displayed on a page
    async addItemNr(nthNumber, amount){
        //select nth product
        let product = await Selector('product-box-component').nth(nthNumber);
        
        //set name and amount
        let name = await product.find('.pbcTitle').innerText;
        await t.typeText(product.find('.product__quantityInput'), amount.toString(), { replace: true });

        //add if new item or add to other products
        this.addItemToObject(name.replace(/ /g,''), amount);

        //click add
        await t.click(product.find('.product__plusButton'));

        //check if added correctly
        await this.checkBasketPopup();
    }

    //used to add the item to this object
    addItemToObject(name, amount){
        let flagNew = true;
        for (var item in this.items) {
            if(this.items[item].name === name.toLowerCase()){
                this.items[item].amount += amount;
                flagNew = false;
            }
        }
        if(flagNew) {
            this.items.push(new Item(name, amount));
        }
    }

    //used to delete the item to this object
    deleteItemFromObject(name){
        var array = [];
        for (var i in this.items){
            array.push(this.items[i].name);
        }
        const index = array.indexOf(name.toLowerCase().replace(/ /g, ''));
        if (index !== -1) {
            this.items.splice(index, 1);
        }
    }
    
    //delete the nth() item from the cart page
    async deleteItem(nthNumber){
        //select nth product
        let product = await Selector('.cart__item').nth(nthNumber);

        //get name for check
        let name = await product.find('.cart__itemTitle').innerText;

        //delete item
        this.deleteItemFromObject(name);

        //click delete
        await t.click(product.find('div').withAttribute('click.delegate', 'deleteItem(item)'));

        //check if added correctly
        await this.checkBasketPopup();
    }

    //delete the nth() item from the cart popup
    async deleteItemPopup(nthNumber){
        //select nth product
        let product = await Selector('.orderLine').nth(nthNumber);

        //get name for check
        let name = await product.find('.item--title').innerText;

        //delete item
        this.deleteItemFromObject(name);

        //click delete
        await t
            .click(Selector('#cart-icon-div'))
            .click(product.find('.order-delete'))
            .click(Selector('#cart-icon-div'));

        //check if added correctly
        await this.checkBasketPopup();
    }

    //check if the object cart corresponds with the shoppingcart basket
    async checkBasketPopup(){
        await t.click(Selector('#cart-icon-div'));
        let itemsSelector = await Selector('.orderLineWrapper').find('.orderLine');
        let itemsPopup = [];
        for (let i = 0; i < await itemsSelector.count; i++){
            let title = await itemsSelector.nth(i).find('.item--title').innerText;
            let amount = await itemsSelector.nth(i).find('input').withAttribute('value.bind', 'item.quantity & validate').value;
            itemsPopup.push(new Item(title, amount));
        }
        //console.log(itemsPopup);
        //console.log(this.items);
        await t.expect(itemsPopup).eql(this.items);
        
        //unclick popup.
        await t.click(Selector('.transparentBackground'));
    }
}

