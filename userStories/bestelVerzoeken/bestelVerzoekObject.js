import { ClientFunction, Selector, t } from 'testcafe';

const getWindowLocation = ClientFunction(() => window.location);


class Item {
    constructor(name, amount){
        this.name = name.toLowerCase().replace(/\n/g, '').replace(/ /g, '').replace(/&/g, '');
        this.amount = parseInt(amount);
    }
}

//TODO
export default class BestelVerzoekObject {
    constructor(nth) {
        this.orderItems = [ new Item('AcerAspireEX21', 2),
                            new Item('LenovoTest123', 1),
                            new Item('combivoordeel:playstation4ipadproipodnano', 1)];
        this.nth = nth;
    }

    async clickOrder(){
        //console.log(this.nth);
        //console.log(await Selector('.masterView > .row').nth(this.nth).innerText);
        await t
            .expect(Selector('.masterView > .row').nth(this.nth).exists).ok()
            .click(Selector('.masterView > .row').nth(this.nth));
    }

    async checkContentRequestBox(){
        await t.expect(Selector('.contextRequestBox ').exists).ok();
    }

    async checkNotExistingBoxes(){
        await t
            .expect(Selector('.addToRequestBox').exists).notOk()
            .expect(Selector('.contextRequestBox').exists).notOk();
    }

    async checkExistingBoxes(){
        await t
            .expect(Selector('.addToRequestBox').exists).ok()
            .expect(Selector('.contextRequestBox').exists).notOk();
    }

    async testConversatieHistorie(){
        await t
            .expect(Selector('.ecomDialog').exists).notOk()
            .click(Selector('button').withAttribute('open-dialog-on-click', "dialog-data.bind: order; dialog.bind: 'orderRequestDialog';"))
            .expect(Selector('.ecomDialog').exists).ok()
            .click(Selector('button').withAttribute('click.trigger', 'controller.cancel()'))
            .expect(Selector('.ecomDialog').exists).notOk();
    }

    //add a bundle to the bestelverzoek
    async addBundle(nthNumber, amount){
        //ContentRequestBox needs to be there first

        await t.navigateTo('https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops/555');
        await t
            .expect(Selector('.bundleProduct').count).notEql(0, { timeout: 5000 })
            .expect(Selector('.bundleProduct').count).gte(1);
        let bundle = await Selector('.bundleProduct').nth(nthNumber);
        let bundleItems = await bundle.find('.bundleProduct__product');
        let bundleItemsCount = await bundleItems.count;

        await this.checkContentRequestBox();

        await t.typeText(bundle.find('.product__quantityInput'), amount.toString(), { replace: true });
        let name = 'combivoordeel:';
        for (let i = 0; i < await bundleItemsCount; i++){
            let title = bundleItems.nth(i).find('.product__title');
            name += await title.innerText;
            let quantity = await bundleItems.nth(i).find('.bundleCount');
            if (await quantity.exists){
                await t.expect(await quantity.innerText).notEql('');
                //console.log(await quantity.innerText);
                name += ' (' + await quantity.innerText + ')';
            }
        }
        //add if new item or add to other products
        this.addItemToObject(name, amount);

        //click add
        await t.click(bundle.find('.product__plusButton'));

        //check if added correctly
        await this.checkOrder();
    }

    //add 'amount' of an item to the cart based on being nth() displayed on a page
    async addItemNr(nthNumber, amount){

        await this.checkContentRequestBox();

        //nav to overzicht with items
        await t.navigateTo('https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops');

        //select nth product
        let product = await Selector('product-box-component').nth(nthNumber);
        
        //set name and amount
        let name = await product.find('.pbcTitle').innerText;
        await t.typeText(product.find('.product__quantityInput'), amount.toString(), { replace: true });

        //add if new item or add to other products
        this.addItemToObject(name.replace(/ /g,''), amount);

        //click add
        await t.click(product.find('.product__plusButton'));

        //check if added correctly
        await this.checkOrder();
    }

    //used to add the item to this object
    addItemToObject(name, amount){
        let flagNew = true;
        for (var item in this.orderItems) {
            if(this.orderItems[item].name === name.toLowerCase()){
                this.orderItems[item].amount += amount;
                flagNew = false;
            }
        }
        if(flagNew) {
            this.orderItems.push(new Item(name, amount));
        }
    }

    //used to delete the item to this object
    deleteItemFromObject(name){
        var array = [];
        for (var i in this.orderItems){
            array.push(this.orderItems[i].name);
        }
        const index = array.indexOf(name.toLowerCase().replace(/ /g, ''));
        if (index !== -1) {
            this.orderItems.splice(index, 1);
        }
    }
    
    //delete the nth() item from the cart page
    async deleteItem(nthNumber){

        await t.click(Selector('div').withAttribute('click.delegate', 'redirectWithSelectedRequest()'));

        //select nth product
        let product = await Selector('.cart__item').nth(nthNumber);

        //get name for object
        let name = '';
        if(await product.nth(nthNumber).find('.cart__itemTitle').exists){
            name = await product.nth(nthNumber).find('.cart__itemTitle').innerText;
        }else{
            name = 'COMBIVOORDEEL:' + await product.find('.cart__bigRedSquare-inner > p').innerText;
            name = name.replace(/&/g, '');
        }

        //delete item
        this.deleteItemFromObject(name);
        
        //click delete
        await t.click(product.find('div').withAttribute('click.delegate', 'deleteItem(orderLine)'));

        //check if added correctly
        await this.checkOrder();
    }

    async toggleContextRequest(){
        await t.click(Selector('label').withAttribute('for', 'addToContextRequest'));
    }

    async deleteContextRequest(){
        await t.click(Selector('.contextBox__close'));
    }

    //check if the object cart corresponds with the shoppingcart basket
    async checkOrder(){
        await t.click(Selector('div').withAttribute('click.delegate', 'redirectWithSelectedRequest()'));
        let items = await Selector('.orderOverview__orderLines');
        let itemsSelector = await items.find('.cart__item');

        await t.expect(itemsSelector.count).notEql(0, { timeout: 20000 });
        //console.log(await itemsSelector.count);
        let orderItems = [];
        for (let i = 0; i < await itemsSelector.count; i++){
            let title = '';
            if(await itemsSelector.nth(i).find('.cart__itemTitle').exists){
                title = await itemsSelector.nth(i).find('.cart__itemTitle').innerText;
            }else{
                title = 'COMBIVOORDEEL:' + await itemsSelector.nth(i).find('.cart__bigRedSquare-inner > p').innerText;
            }
            let amount = await itemsSelector.nth(i).find('.cart__item-input').value;
            orderItems.push(new Item(title, amount));
            
        }
        //console.log(orderItems);
        //console.log(this.orderItems);
        await t.expect(orderItems).eql(this.orderItems);
    }
}