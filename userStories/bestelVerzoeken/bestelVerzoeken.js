import { ClientFunction, Selector, t} from 'testcafe';
import BestelVerzoekObject from './bestelVerzoekObject';
import WinkelwagenObject from '../Winkelwagen/WinkelwagenObject';

const getWindowLocation = ClientFunction(() => window.location);

fixture `bestelVerzoeken` 
.page `https://test-winkel.bnc.nl/#/my-account/order-request-overview`;

test('check unchecked order', async t => {
    //go to order
    let testBestelverzoek = new BestelVerzoekObject(1);
    await testBestelverzoek.clickOrder();

    //check history button/terug button
    await testBestelverzoek.testConversatieHistorie();

    //check contextrequestbox + addToRequest not existing
    await testBestelverzoek.checkNotExistingBoxes();
})

test('check approved order', async t => {
    //go to order
    let testBestelverzoek = new BestelVerzoekObject(2);
    await testBestelverzoek.clickOrder();

    //check history button/terug button
    await testBestelverzoek.testConversatieHistorie();
    
    //check contextrequestbox + addToRequest not existing
    await testBestelverzoek.checkNotExistingBoxes();
})

test('check rejected order', async t => {
    let testBestelverzoek = new BestelVerzoekObject(3);
    await testBestelverzoek.clickOrder();
        
    //check history button/terug button
    await testBestelverzoek.testConversatieHistorie();
    
    //check contextrequestbox + addToRequest are existing
    await testBestelverzoek.checkExistingBoxes();

    //check requestBox nav to home + contextRequestsBox exists
    await t.click('.addToRequestBox ');
    let location = await getWindowLocation();   
    await t
        .expect(location.href).eql('https://test-winkel.bnc.nl/#/')
        .expect(Selector('.contextRequestBox ').exists).ok();

    //add item to order + check if contextRequestsBox still exists
    await t
        .navigateTo('https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops')
        .expect(Selector('.contextRequestBox ').exists).ok();
})

test('test adding orderItem', async t => {
    let testBestelverzoek = new BestelVerzoekObject(3);
    await testBestelverzoek.clickOrder();

    //check contextrequestbox + addToRequest not existing
    await testBestelverzoek.checkExistingBoxes();

    await t.click('.addToRequestBox');
    await testBestelverzoek.addItemNr(5, 6);
    await testBestelverzoek.addItemNr(7, 2);
})

test('test adding bundle', async t => {
    let testBestelverzoek = new BestelVerzoekObject(3);
    await testBestelverzoek.clickOrder();

    //check contextrequestbox + addToRequest not existing
    await testBestelverzoek.checkExistingBoxes();

    await t.click('.addToRequestBox');
    await testBestelverzoek.addItemNr(5, 6);
    await testBestelverzoek.addBundle(1, 2);
})

test('test deleting orderItem', async t => {
    let testBestelverzoek = new BestelVerzoekObject(3);
    await testBestelverzoek.clickOrder();

    //check history button/terug button
    await testBestelverzoek.testConversatieHistorie();

    //check contextrequestbox + addToRequest not existing
    await testBestelverzoek.checkExistingBoxes();

    await t.click('.addToRequestBox ');
    await testBestelverzoek.addItemNr(5, 6);
    await testBestelverzoek.addItemNr(2, 3);
    await testBestelverzoek.deleteItem(3);
    await testBestelverzoek.deleteItem(3);
    await testBestelverzoek.deleteItem(2);
})

test('test toggle contextBox', async t => {
    let ww = new WinkelwagenObject();
    let testBestelverzoek = new BestelVerzoekObject(3);
    await testBestelverzoek.clickOrder();

    //check history button/terug button
    await testBestelverzoek.testConversatieHistorie();

    //check contextrequestbox + addToRequest not existing
    await testBestelverzoek.checkExistingBoxes();

    await t.click('.addToRequestBox');

    await testBestelverzoek.addItemNr(5, 6);
    await testBestelverzoek.addItemNr(7, 2);
    await testBestelverzoek.deleteItem(2)

    await testBestelverzoek.toggleContextRequest();

    await t.navigateTo('https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops/');
    await ww.addItemNr(2, 4);
    //TODO uncomment if 3x x3 error is gone
    //await t.navigateTo('https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops/555');
    //await ww.addBundle(0, 3);
    await ww.deleteItemPopup(2, 2);

    await testBestelverzoek.toggleContextRequest();
    
    await testBestelverzoek.addItemNr(2, 2);
    await testBestelverzoek.deleteItem(1);

    await testBestelverzoek.deleteContextRequest();

    await t.navigateTo('https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops/');
    await ww.addItemNr(5, 2);
    //TODO uncomment if 3x x3 error is gone
    //await t.navigateTo('https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops/555');
    //await ww.addBundle(1, 1);
    await ww.deleteItemPopup(2, 2);    
})