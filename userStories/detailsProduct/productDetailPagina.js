import { Selector } from 'testcafe';


fixture `ProductDetailPagina` 
.page `https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops/90`;

//check specsContainer exists, then checks if there are more specsContainers after clicking 'meer specificaties'
//then check if there are less when you press minder specificaties
test('specs aanwezig', async t => {
    await t.expect(Selector('.product__specsContainer').exists).ok();
    let countBefore = await Selector('.product__specsContainer').count;
    await t.click(Selector('div').withAttribute('click.delegate', 'toggleShowAllSpecs()'));
    let countAfter = await Selector('.product__specsContainer').count;
    await t.expect(countBefore).lte(countAfter);
    await t.click(Selector('div').withAttribute('click.delegate', 'toggleShowAllSpecs()'));
    countBefore = await Selector('.product__specsContainer').count;
    await t.expect(countBefore).lte(countAfter);
});

//check if the page has key selling points in the main product box
test('ksp aanwezig', async t => {
    //TODO fix second related #product90 o.0
    await t.expect(await Selector('#product90 > .product__ksp')).ok();
});

//check if ecom dialog pops up after clicking the sharebutton
test('share? aanwezig', async t => {
    await t.expect(Selector('.ecomDialog').exists).notOk();
    await t.click(Selector('.shareIcon_absolute'));
    await t.expect(Selector('.ecomDialog').exists).ok();
});

//check length text before and after clicking on more text. Then click and check again.
test('lees meer', async t => {
    let descBefore = await Selector('.product__largeDescription').innerText;
    await t.click(Selector('div').withAttribute('click.delegate', 'toggleShowMoreProductInfo()'));
    let descAfter = await Selector('.product__largeDescription').innerText;
    await t.expect(descBefore.length).lte(descAfter.length);
    //console.log(descBefore.length + '-' + descAfter.length);

    await t.click(Selector('div').withAttribute('click.delegate', 'toggleShowMoreProductInfo()'));
    descBefore = await Selector('.product__largeDescription').innerText;
    await t.expect(descBefore.length).lte(descAfter.length);
    //console.log(descBefore.length + '-' + descAfter.length);

});

test('de plaatjes', async t => {
    //TODO DOE HIER WAT MEE
    await t.click(Selector('div').withAttribute('click.delegate', 'nextItem($event)'));
    await t.click(Selector('div').withAttribute('click.delegate', 'previousItem($event)'));
    
});
