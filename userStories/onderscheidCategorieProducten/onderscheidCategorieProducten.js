import { Selector, t } from 'testcafe';


fixture `Onderscheid categorie producten` 
.page `https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops`;

//filter items by 'voor uw bedrijf' and check if all the filtered items on this page contain the .fa-building element
test('check labels', async t => {
    let filterValue = await Selector('.filter-value').withText('voor uw bedrijf');
    await t.click(filterValue);
    
    let pbc = Selector('product-box-component');
    let pbcCount = await Selector('product-box-component').count;
    for(let i = 0; i < pbcCount-4; i++){
        await t.expect(pbc.nth(i).find('.fa-building').exists).ok();
    }
})
